#!/bin/bash

source bash-uri.sh

set -o xtrace

uri "https://jsonplaceholder.typicode.com/posts/1" "GET" 200 "" ""

tmp_data_file=$(date '+%Y%m%d%H%M%S_bashuri_test')
echo -n '{"title":"foo","body":"bar","userId":1}' |jq > /tmp/$tmp_data_file
uri "https://jsonplaceholder.typicode.com/posts" "POST" 201 "" "" "application/json" "/tmp/$tmp_data_file"

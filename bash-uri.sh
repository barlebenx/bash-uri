#
# Simple function to performe uri access in bash with curl
#

function uri(){
  # Perform api request en return response body
  #
  # $1 : url, https://jsonplaceholder.typicode.com/posts for example
  # $2 : method, GET / POST / PUT / DELETE
  # $3 : Expected http status code, if you want to ensure it
  # $4 : basic auth user
  # $5 : basic auth password
  # $6 : content type, "application/json" by default
  # $7 : path to data file
  # $8 : yes / no : if we want to disable cetificate check
  #

  url=$1

  if [[ "$2" == "" ]]
  then
    method="GET"
  else
    method="$2"
  fi
    expected_status_code="$3"

  if [[ "$4" != "" ]] && [[ "$5" != "" ]]
  then
    basic_auth=$(echo -n "$4:$5" | base64)
  else
    basic_auth="anonymous:anonymous"
  fi

  # content_type : application/json ; form-urlencoded ...
  if [[ "$6" != "" ]]
  then
    content_type="$6"
  else
    content_type="application/json"
  fi

  data_file="$7"

  if [[ "$8" == "no" ]]
  then
    insecure="--insecure"
  else
    insecure=""
  fi

  response=$(curl -si \
    -w '\n%{size_header},%{size_download}\n' \
    -X ${method} ${insecure} \
    -H "Content-Type: $content_type" \
    -H "Authorization: Basic $basic_auth" \
    $(if [[ "$data_file" != "" ]]; then printf " -d @${data_file}"; fi) \
    "${url}" \
  )

  if [[ $? -ne 0 ]]
  then
    echo "[FAILED] curl request failed" 1>&2
    exit 2
  fi

  headerSize=$(sed -n 's/^\([0-9]*\),.*$/\1/p' <<< "${response}")
  bodySize=$(sed -n 's/^.*,\([0-9]*\)$/\1/p' <<< "${response}")
  headers="${response:0:${headerSize}}"
  body="${response:${headerSize}:${bodySize}}"

  http_status_code=$(echo -n "$headers" |head -n 1 |awk '{print $2}')

  if [[ "$expected_status_code" != "" ]] && [[ "$http_status_code" != "$expected_status_code" ]]
  then
    echo "[FAILED] Invalid status code : $http_status_code" 1>&2
    exit 2
  fi

  echo "$body"
}
